const csvFilePath1 = '../data/matches.csv'
const csvFilePath2 = '../data/deliveries.csv'
const fs = require("fs")

const csv = require('csvtojson')
csv()
  .fromFile(csvFilePath1)
  .then((matches) => {
    csv().fromFile(csvFilePath2)
      .then((deliveries) => {

        let matchIDs = [];
        let season = 2015

        matches.filter(items => {
          if (items.season == season) {
            matchIDs.push(items.id)
          }
        });

        let overs = (bowler) => {
          let balls = 0;
          deliveries.filter(items => {
            if (matchIDs.includes((items.match_id))) {
              if (items.bowler == bowler) {
                balls += 1;
              }
            }
          });
          return balls / 6;
        }

        const getRunsByBowler = deliveries.reduce((initial, current) => {
          if (matchIDs.includes(current.match_id)) {
            if (!initial[current.bowler]) {
              initial[current.bowler] = Number(current.total_runs);
            } else {
              initial[current.bowler] += Number(current.total_runs);
            }
          }
          return initial;
        }, {});

        let economicalBowler2015 = Object.keys(getRunsByBowler).map(key => {
          return [key, (getRunsByBowler[key] / overs(key)).toFixed(2)];
        })
          .sort(function (a, b) {
            return a[1] - b[1];
          })
          .slice(0, 10);

        let jsonFilePath = __dirname + '/../public/output/4-economicalBowler2015.json';

        fs.writeFile(jsonFilePath, JSON.stringify(economicalBowler2015, null, 2), (err) => {
          if (err) {
            console.log(err);
          }
        })
      })
  });


