const csvFilePath = '../data/matches.csv'
const csv = require('csvtojson')
const fs = require("fs")
csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {

        let teamWonTossAndMatch = jsonObj.reduce((acc, current) => {

            if (current.toss_winner === current.winner) {
                if (acc[current.toss_winner]) {
                    acc[current.toss_winner]++;
                }
                else {
                    acc[current.toss_winner] = 1;
                }
            }
            return acc;
        }, {});

        let jsonFilePath = __dirname + '/../public/output/5-teamWonTossAndMatch.json';

        fs.writeFile(jsonFilePath, JSON.stringify(teamWonTossAndMatch, null, 2), (err) => {
            if (err) {
                console.log(err);
            }
        })
    });



