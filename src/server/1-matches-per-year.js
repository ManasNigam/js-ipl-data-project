const csvFilePath = '../data/matches.csv'
const csv = require('csvtojson')
const fs = require("fs")
csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {

        const matchesPerYear = jsonObj.reduce((acc, current) => {

            if (acc[current.season]) {
                acc[current.season]++;
            }
            else {
                acc[current.season] = 1;
            }
            return acc;
        }, {})

        let jsonFilePath = __dirname + '/../public/output/1-matches-per-year.json';

        fs.writeFile(jsonFilePath, JSON.stringify(matchesPerYear, null, 2), (err) => {
            if (err) {
                console.log(err);
            }
        })
    });





