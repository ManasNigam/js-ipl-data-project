const csvFilePath = '../data/deliveries.csv'
const csv = require('csvtojson')
const fs = require("fs")

csv()
    .fromFile(csvFilePath)
    .then((deliveries) => {


        let dismissalKindArray = [];
        deliveries.map((items) => {
            if (!(items.dismissal_kind === "" ||
                items.dismissal_kind === "run out" ||
                items.dismissal_kind === "retired hurt" ||
                items.dismissal_kind === "obstructing the field")) {
                if (!dismissalKindArray.includes(items.dismissal_kind)) {
                    dismissalKindArray.push(items.dismissal_kind);
                }
            }
        });


        let max = 0;
        let maxArray = {};
        let allDismissals = {};
        deliveries.map((items) => {
            if (dismissalKindArray.includes(items.dismissal_kind)) {
                if (!allDismissals[items.bowler + " " + items.player_dismissed]) {
                    allDismissals[items.bowler + " " + items.player_dismissed] = 1;
                }
                else {
                    allDismissals[items.bowler + " " + items.player_dismissed]++;
                    if (allDismissals[items.bowler + " " + items.player_dismissed] > max) {
                        max = allDismissals[items.bowler + " " + items.player_dismissed];
                        maxArray["Batsman"] = items.player_dismissed;
                        maxArray["Bowler"] = items.bowler;
                        maxArray["Dismissals"] = max;
                    }
                }
            }
        });

        let jsonFilePath = __dirname + '/../public/output/8-highestDismissal.json';

        fs.writeFile(jsonFilePath, JSON.stringify(maxArray, null, 2), (err) => {
            if (err) {
                console.log(err);
            }
        });
    });





