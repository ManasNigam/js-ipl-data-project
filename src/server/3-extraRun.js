const csvFilePath1 = '../data/matches.csv'
const csvFilePath2 = '../data/deliveries.csv'
const fs = require("fs")
const csv = require('csvtojson');
csv()
    .fromFile(csvFilePath1)
    .then((jsonObj1) => {
        csv().fromFile(csvFilePath2)
            .then((jsonObj2) => {

                let season = 2016
                let matchIDs = [];
                jsonObj1.filter(items => {
                    if (items.season == season) {
                        matchIDs.push(items.id)
                    }
                });
                const getExtraRuns = jsonObj2.reduce((initial, current) => {
                    if (matchIDs.includes(current.match_id)) {
                        if (!initial[current.bowling_team])
                            initial[current.bowling_team] = Number(current.extra_runs);
                        else
                            initial[current.bowling_team] += Number(current.extra_runs);
                    }
                    return initial;
                }, {});


                let jsonFilePath = __dirname + '/../public/output/3-extraRun.json';

                fs.writeFile(jsonFilePath, JSON.stringify(getExtraRuns, null, 2), (err) => {
                    if (err) {
                        console.log(err);
                    }
                })

            })
    });