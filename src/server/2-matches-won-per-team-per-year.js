// Number of matches won per team per year in IPL.
const csvFilePath = '../data/matches.csv'
const csv = require('csvtojson')

const fs = require("fs")
csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {

        let matchesWonPerTeamPerYear = {}
        let teamArr = []

        teamArr = jsonObj.reduce((initial, current) => {
            if (!initial.includes(current.winner)) {
                if (current.result == "normal") {
                    initial.push(current.winner);
                }
            }
            return initial;
        }, [])


        let matchesWonPerTeam = (team) => {
            let count = {}

            jsonObj.reduce((initial, current) => {
                if (current.winner == team && current.result == "normal") {
                    initial.push(current.season);
                }
                return initial;
            }, []).map((items) => { count[items] = (count[items] || 0) + 1; });
            return count;
        }

        let result = teamArr.map((items) => {
            matchesWonPerTeamPerYear[items] = matchesWonPerTeam(items)
            return matchesWonPerTeamPerYear;
        });

        let jsonFilePath = __dirname + '/../public/output/2-matches-won-per-team-per-year.json';
        fs.writeFile(jsonFilePath, JSON.stringify(result, null, 2), (err) => {
            if (err) {
                console.log(err);
            }
        })
    });