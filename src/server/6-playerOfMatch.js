const csvFilePath = '../data/matches.csv'
const csv = require('csvtojson')
const fs = require("fs")

csv()
    .fromFile(csvFilePath)
    .then((matches) => {

        //array for each season
        let yearArray = matches.reduce((initial, current) => {
            if (!initial.includes(current.season)) {
                initial.push(current.season);
            }
            return initial;
        }, [])


        let playerOfMatch = (season) => {
            let player = matches.reduce((acc, current) => {
                if (current.season == season) {
                    if (acc[current.player_of_match]) {
                        acc[current.player_of_match]++;
                    } else {
                        acc[current.player_of_match] = 1;
                    }
                }
                return acc;
            }, []);
            let MostPlayerOfMatch = Object.entries(player).sort((item1, item2) => {
                return item1[1] - item2[1];
            });
            return (MostPlayerOfMatch[MostPlayerOfMatch.length - 1]);
        };


        // for each season most "player of match" award for player 
        let playerofMatcheachSeason = yearArray.reduce((acc, current) => {
            acc[current] = playerOfMatch(current)
            return acc;
        }, {});

        let jsonFilePath = __dirname + '/../public/output/6-playerOfMatch.json';

        fs.writeFile(jsonFilePath, JSON.stringify(playerofMatcheachSeason, null, 2), (err) => {
            if (err) {
                console.log(err);
            }
        })


    });



