const csvFilePath1 = '../data/matches.csv'
const csvFilePath2 = '../data/deliveries.csv'
const fs = require("fs")

const csv = require('csvtojson')
csv()
  .fromFile(csvFilePath1)
  .then((jsonObj1) => {
    csv().fromFile(csvFilePath2)
      .then((jsonObj2) => {


        function overs(bowler) {
          let balls = 0;
          jsonObj2.filter(items => {
            if (items.is_super_over == 1) {
              if (items.bowler == bowler) {
                balls += 1;
              }
            }
          });
          return balls / 6;
        }

        let economicalBowlers = []
        economicalBowlers = jsonObj2.reduce((initial, current) => {
          if (current.is_super_over == 1) {
            if (!initial[current.bowler]) {
              initial[current.bowler] = Number(current.total_runs);
              economicalBowlers.push(initial)
            }
            else
              initial[current.bowler] += Number(current.total_runs);
            economicalBowlers.push(initial)
          }
          return initial;
        }, [])


        let bestBowlerinSuperOvers = Object.keys(economicalBowlers).map(function (key) {
          return [key, (economicalBowlers[key] / overs(key)).toFixed(1)];
        })
          .sort(function (a, b) { return a[1] - b[1]; });


        let jsonFilePath = __dirname + '/../public/output/9-bestBowlerinSuperOvers.json';

        fs.writeFile(jsonFilePath, JSON.stringify(bestBowlerinSuperOvers[0], null, 2), (err) => {
          if (err) {
            console.log(err);
          }
        })


      })
  });


